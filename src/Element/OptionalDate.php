<?php

namespace Drupal\optional_date\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\Element\Datetime;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an optional_date element.
 *
 * @FormElement("optional_date")
 */
class OptionalDate extends Datetime {

  /**
   * Defines the timezone that dates should be stored in.
   */
  const STORAGE_TIMEZONE = 'UTC';

  /**
   * {@inheritdoc}
   */
  public static function processDatetime(&$element, FormStateInterface $form_state, &$complete_form) {
    $element = parent::processDatetime($element, $form_state, $complete_form);

    if ($element['#date_time_is_empty']) {
      $element['time']['#value'] = '';
    }

    if ($element['#date_time_is_optional']) {
      $element['time']['#required'] = FALSE;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateDatetime(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);

    if (!is_array($value)) {
      return;
    }

    if (empty($value['date']) && empty($value['time'])) {
      return;
    }

    $dateFormat = $element['#date_date_element'] != 'none' ? static::getHtml5DateFormat($element) : '';
    $timeFormat = $element['#date_time_element'] != 'none' ? static::getHtml5TimeFormat($element) : '';

    $format = static::formatExample(trim($dateFormat . ' ' . $timeFormat));
    $message = NULL;
    $title = !empty($element['#title']) ? $element['#title'] : '';

    if ($value['object'] === NULL) {
      $message = t('The %title date is invalid. The expected format is %format.',
        ['%title' => $title, '%format' => $format]);
    }
    elseif (empty($value['date']) && $element['#required']) {
      $message = t('The %title date is required. The expected format is %format.',
        ['%title' => $title, '%format' => $format]);
    }
    elseif (empty($value['time']) && !$element['#date_time_is_optional']) {
      $message = t('The %title time is required. The expected format is %format.',
        ['%title' => $title, '%format' => $format]);
    }

    if ($message) {
      $form_state->setError($element, $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = $input;

    if ($value !== FALSE) {
      if (!empty($value['date']) && empty($value['time'])) {
        $value['time'] = $element['#date_time_default'];

        $element['#date_time_is_empty'] = TRUE;
        $element['#date_timezone'] = static::STORAGE_TIMEZONE;
      }
      else {
        $element['#date_time_is_empty'] = FALSE;
        $element['#date_timezone'] = date_default_timezone_get();
      }
    }

    $value = parent::valueCallback($element, $value, $form_state);

    if ($element['#date_time_is_empty']) {
      $value['time'] = '';
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#date_increment'] = 60;
    $info['#date_time_default'] = '00:00:00';
    $info['#date_time_is_empty'] = FALSE;
    $info['#date_time_is_optional'] = FALSE;

    return $info;
  }

}
