<?php

namespace Drupal\optional_date\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\Date;

/**
 * A views sort plugin.
 *
 * @ViewsSort("optional_date")
 */
class OptionalDate extends Date {}
