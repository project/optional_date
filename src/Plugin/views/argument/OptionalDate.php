<?php

namespace Drupal\optional_date\Plugin\views\argument;

use Drupal\views\Plugin\views\argument\Date;

/**
 * A views argument plugin.
 *
 * @ViewsArgument("optional_date")
 */
class OptionalDate extends Date {}
