<?php

namespace Drupal\optional_date\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\optional_date\Element\OptionalDate;

/**
 * Plugin implementation of the 'Default' widget for optional_date_range fields.
 *
 * @FieldWidget(
 *   id = "optional_date_range",
 *   label = @Translation("Optional Date Range"),
 *   field_types = {
 *     "optional_date_range"
 *   }
 * )
 */
class OptionalDateRangeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#element_validate'][] = [$this, 'validateRange'];
    $element['#theme_wrappers'][] = 'fieldset';

    $element['start_date'] = [
      '#default_value' => NULL,
      '#required' => $element['#required'],
      '#title' => $this->t('Start'),
      '#type' => 'optional_date',
    ];

    if ($this->getFieldSetting('time_is_optional')) {
      $element['start_date']['#date_time_is_optional'] = TRUE;
    }

    if ($items[$delta]->start_time_is_empty) {
      $element['start_date']['#date_time_is_empty'] = TRUE;
      $element['start_date']['#date_timezone'] = OptionalDate::STORAGE_TIMEZONE;
    }
    else {
      $element['start_date']['#date_timezone'] = date_default_timezone_get();
    }

    if ($items[$delta]->start_timestamp) {
      $element['start_date']['#default_value'] = DrupalDateTime::createFromTimestamp($items[$delta]->start_timestamp);
    }

    $element['end_date'] = [
      '#date_time_default' => '23:59:59',
      '#default_value' => NULL,
      '#required' => $element['#required'],
      '#title' => $this->t('End'),
      '#type' => 'optional_date',
    ];

    if ($this->getFieldSetting('end_is_optional')) {
      $element['end_date']['#required'] = FALSE;
    }

    if ($this->getFieldSetting('time_is_optional')) {
      $element['end_date']['#date_time_is_optional'] = TRUE;
    }

    if ($items[$delta]->end_time_is_empty) {
      $element['end_date']['#date_time_is_empty'] = TRUE;
      $element['end_date']['#date_timezone'] = OptionalDate::STORAGE_TIMEZONE;
    }
    else {
      $element['end_date']['#date_timezone'] = date_default_timezone_get();
    }

    if ($items[$delta]->end_timestamp) {
      $element['end_date']['#default_value'] = DrupalDateTime::createFromTimestamp($items[$delta]->end_timestamp);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      $startDate = $item['start_date']['object'];

      if ($startDate instanceof DrupalDateTime && !$startDate->hasErrors()) {
        $item['end_time_is_empty'] = NULL;
        $item['end_timestamp'] = NULL;
        $item['start_time_is_empty'] = empty($item['start_date']['time']);
        $item['start_timestamp'] = $startDate->getTimestamp();

        $endDate = $item['end_date']['object'];

        if ($endDate instanceof DrupalDateTime && !$endDate->hasErrors()) {
          $item['end_time_is_empty'] = empty($item['end_date']['time']);
          $item['end_timestamp'] = $endDate->getTimestamp();
        }
      }
      else {
        $item = NULL;
      }
    }

    return $values;
  }

  /**
   * Callback to validate the start and end dates.
   *
   * The dates are compared to make sure the start is before the end.
   * The dates are compared to make sure the start is populated if the end is.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateRange(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $end_date = $element['end_date']['#value']['object'];
    $start_date = $element['start_date']['#value']['object'];

    if ($start_date instanceof DrupalDateTime && $end_date instanceof DrupalDateTime) {
      if ($start_date->getTimestamp() !== $end_date->getTimestamp()) {
        $interval = $start_date->diff($end_date);
        if ($interval->invert === 1) {
          $form_state->setError($element, $this->t('The @title end date cannot be before the start date.', ['@title' => $element['#title']]));

          return;
        }
      }
    }

    $end_time = $element['end_date']['#value']['time'];
    $start_time = $element['start_date']['#value']['time'];

    if (empty($start_time) && !empty($end_time)) {
      $form_state->setError($element, $this->t('The @title start time is required if the end time is populated.', ['@title' => $element['#title']]));
    }
  }

}
