<?php

namespace Drupal\optional_date\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\optional_date\Element\OptionalDate;

/**
 * Plugin implementation of the 'Default' widget for optional_date fields.
 *
 * @FieldWidget(
 *   id = "optional_date",
 *   label = @Translation("Optional Date"),
 *   field_types = {
 *     "optional_date"
 *   }
 * )
 */
class OptionalDateWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#theme_wrappers'][] = 'fieldset';

    $element['date'] = [
      '#default_value' => NULL,
      '#required' => $element['#required'],
      '#type' => 'optional_date',
    ];

    if ($this->getFieldSetting('time_is_optional')) {
      $element['date']['#date_time_is_optional'] = TRUE;
    }

    if ($items[$delta]->time_is_empty) {
      $element['date']['#date_time_is_empty'] = TRUE;
      $element['date']['#date_timezone'] = OptionalDate::STORAGE_TIMEZONE;
    }
    else {
      $element['date']['#date_timezone'] = date_default_timezone_get();
    }

    if ($items[$delta]->timestamp) {
      $element['date']['#default_value'] = DrupalDateTime::createFromTimestamp($items[$delta]->timestamp);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      $date = $item['date']['object'];

      if ($date instanceof DrupalDateTime && !$date->hasErrors()) {
        $item['time_is_empty'] = empty($item['date']['time']);
        $item['timestamp'] = $date->getTimestamp();
      }
      else {
        $item = NULL;
      }
    }

    return $values;
  }

}
