<?php

namespace Drupal\optional_date\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for 'Optional Date Range field formatter' plugin implementations.
 */
abstract class OptionalDateRangeFormatterBase extends OptionalDateFormatterBase {
  /**
   * Defines the default display.
   */
  const DISPLAY_DEFAULT = 'both';

  /**
   * Defines the 'only start date' display.
   */
  const DISPLAY_ONLY_START = 'start';

  /**
   * Defines the default display.
   */
  const DISPLAY_ONLY_END = 'end';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'display' => static::DISPLAY_DEFAULT,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['display'] = [
      '#default_value' => $this->getSetting('display'),
      '#description' => $this->t("Choose a display option. Both dates will only be displayed if end date has a value."),
      '#options' => [
        static::DISPLAY_DEFAULT => 'Both dates',
        static::DISPLAY_ONLY_START => 'Only start date',
        static::DISPLAY_ONLY_END => 'Only end date',
      ],
      '#title' => $this->t('Display'),
      '#type' => 'select',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $display = $this->getSetting('display');
    $format = '';

    $startDateTime = new DrupalDateTime();
    $endDateTime = DrupalDateTime::createFromTimestamp($startDateTime->getTimestamp() + 86400);

    if ($display !== static::DISPLAY_ONLY_END) {
      $format = $format . $startDateTime->format($this->getFormat(FALSE));
    }

    if ($display === static::DISPLAY_DEFAULT) {
      $format = $format . '–';
    }

    if ($display !== static::DISPLAY_ONLY_START) {
      $format = $format . $endDateTime->format($this->getFormat(FALSE));
    }

    $summary = parent::settingsSummary();
    $summary['format'] = $this->t('Format: @format', ['@format' => $format]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $display = $this->getSetting('display');
    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->start_timestamp) && $display !== static::DISPLAY_ONLY_END) {
        $elements[$delta][] = $this->getElement($item->start_timestamp, $item->start_time_is_empty);
      }

      if (!empty($item->start_timestamp) && !empty($item->end_timestamp) && $display === static::DISPLAY_DEFAULT) {
        $elements[$delta][] = ['#markup' => '<span>–</span>'];
      }

      if (!empty($item->end_timestamp) && $display !== static::DISPLAY_ONLY_START) {
        $elements[$delta][] = $this->getElement($item->end_timestamp, $item->end_time_is_empty);
      }
    }

    return $elements;
  }

}
