<?php

namespace Drupal\optional_date\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin for the 'Custom' formatter for optional_date_range fields.
 *
 * @FieldFormatter(
 *   id = "optional_date_range_custom",
 *   label = @Translation("Custom"),
 *   field_types = {
 *     "optional_date_range"
 *   }
 * )
 */
class OptionalDateRangeCustomFormatter extends OptionalDateRangeFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_format' => DateFormat::load('html_date')->getPattern(),
      'separator' => ' ',
      'time_before_date' => FALSE,
      'time_format' => DateFormat::load('html_time')->getPattern(),
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormat($timeIsEmpty) {
    $dateFormat = $this->getSetting('date_format');

    if ($timeIsEmpty) {
      return $dateFormat;
    }

    $separator = $this->getSetting('separator');
    $timeBeforeDate = $this->getSetting('time_before_date');
    $timeFormat = $this->getsetting('time_format');

    if ($timeBeforeDate) {
      return $timeFormat . $separator . $dateFormat;
    }

    return $dateFormat . $separator . $timeFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date format'),
      '#description' => $this->t('See <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">the documentation for PHP date formats</a>.'),
      '#default_value' => $this->getSetting('date_format'),
    ];

    $form['time_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time format'),
      '#description' => $this->t('See <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">the documentation for PHP date formats</a>.'),
      '#default_value' => $this->getSetting('time_format'),
    ];

    $form['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Separator'),
      '#description' => $this->t('The string to separate the date and time.'),
      '#default_value' => $this->getSetting('separator'),
    ];

    $form['time_before_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place time before date'),
      '#default_value' => $this->getSetting('time_before_date'),
    ];

    return $form;
  }

}
