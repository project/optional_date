<?php

namespace Drupal\optional_date\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\TimestampAgoFormatter;

/**
 * Plugin for the 'Time ago' formatter for optional_date fields.
 *
 * @FieldFormatter(
 *   id = "optional_date_time_ago",
 *   label = @Translation("Time ago"),
 *   field_types = {
 *     "optional_date"
 *   }
 * )
 */
class OptionalDateTimeAgoFormatter extends TimestampAgoFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->timestamp)) {
        $elements[$delta] = parent::formatTimestamp($item->timestamp);
      }
    }

    return $elements;
  }

}
