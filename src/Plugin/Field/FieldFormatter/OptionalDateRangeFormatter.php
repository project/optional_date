<?php

namespace Drupal\optional_date\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin for the 'Default' formatter for optional_date_range fields.
 *
 * @FieldFormatter(
 *   id = "optional_date_range",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "optional_date_range"
 *   }
 * )
 */
class OptionalDateRangeFormatter extends OptionalDateRangeFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format' => static::DEFAULT_FORMAT,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormat($timeIsEmpty) {
    $format = $this->getSetting('format');

    return DateFormat::load($format)->getPattern();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['format'] = [
      '#default_value' => $this->getSetting('format'),
      '#description' => $this->t("Choose a format for displaying the date/time. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date."),
      '#options' => $this->getFormats(),
      '#title' => $this->t('Format'),
      '#type' => 'select',
    ];

    return $form;
  }

}
