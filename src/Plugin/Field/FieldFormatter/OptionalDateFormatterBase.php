<?php

namespace Drupal\optional_date\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\optional_date\Element\OptionalDate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for 'Optional Date field formatter' plugin implementations.
 */
abstract class OptionalDateFormatterBase extends FormatterBase {
  /**
   * Defines the default format.
   */
  const DEFAULT_FORMAT = 'fallback';

  /**
   * Defines the ISO date format.
   */
  const ISO_DATE_FORMAT = 'Y-m-d';

  /**
   * Defines the ISO datetime format.
   */
  const ISO_DATETIME_FORMAT = "Y-m-d\TH:i:s\Z";

  /**
   * The date format entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateFormatStorage;

  /**
   * Constructs a new DateTimeDefaultFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $date_format_storage
   *   The date format entity storage.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityStorageInterface $date_format_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->dateFormatStorage = $date_format_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('date_format')
    );
  }

  /**
   * Creates a render array from a timestamp.
   *
   * @param int $timestamp
   *   A timestamp.
   * @param bool $timeIsEmpty
   *   Indicates if the time is optional.
   *
   * @return array
   *   A render array.
   */
  protected function getElement($timestamp, $timeIsEmpty) {
    $format = $this->getFormat($timeIsEmpty);
    $isoFormat = $this->getIsoFormat($timeIsEmpty);
    $timezone = $this->getTimezone($timeIsEmpty);

    $datetime = DrupalDateTime::createFromTimestamp($timestamp);
    $datetime->setTimezone(timezone_open($timezone));

    return [
      '#attributes' => [
        'datetime' => $datetime->format($isoFormat),
      ],
      '#text' => $datetime->format($format),
      '#theme' => 'time',
    ];
  }

  /**
   * Gets the format used to display the date.
   *
   * @param bool $timeIsEmpty
   *   Indicates if the time is optional.
   *
   * @return string
   *   A date format.
   */
  abstract protected function getFormat($timeIsEmpty);

  /**
   * Gets the ISO format used to display the date.
   *
   * @param bool $timeIsEmpty
   *   Indicates if the time is optional.
   *
   * @return string
   *   An ISO date format.
   */
  protected function getIsoFormat($timeIsEmpty) {
    return $timeIsEmpty ? static::ISO_DATE_FORMAT : static::ISO_DATETIME_FORMAT;
  }

  /**
   * Gets a set of formats that can be used to display the date.
   *
   * @return array
   *   An array.
   */
  protected function getFormats() {
    $dateTime = new DrupalDateTime();
    $formats = $this->dateFormatStorage->loadMultiple();
    $options = [];

    foreach ($formats as $id => $format) {
      $options[$id] = $format->label() . ' (' . $dateTime->format($format->getPattern()) . ')';
    }

    return $options;
  }

  /**
   * Gets the timezone used to display the date.
   *
   * @param bool $timeIsEmpty
   *   Indicates if the time is optional.
   *
   * @return string
   *   A timezone.
   */
  protected function getTimezone($timeIsEmpty) {
    return $timeIsEmpty ? OptionalDate::STORAGE_TIMEZONE : date_default_timezone_get();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $dateTime = new DrupalDateTime();
    $format = $dateTime->format($this->getFormat(FALSE));

    $summary = parent::settingsSummary();
    $summary['format'] = $this->t('Format: @format', ['@format' => $format]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->timestamp)) {
        $elements[$delta] = $this->getElement($item->timestamp, $item->time_is_empty);
      }
    }

    return $elements;
  }

}
