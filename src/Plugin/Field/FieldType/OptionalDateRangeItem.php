<?php

namespace Drupal\optional_date\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the optional_date_range field type.
 *
 * @FieldType(
 *   id = "optional_date_range",
 *   label = @Translation("Optional Date Range"),
 *   default_widget = "optional_date_range",
 *   default_formatter = "optional_date_range",
 * )
 */
class OptionalDateRangeItem extends FieldItemBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'end_is_optional' => FALSE,
      'time_is_optional' => FALSE,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $end_timestamp = \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365);
    $start_timestamp = $end_timestamp - (86400 / 2);

    return [
      'end_time_is_empty' => FALSE,
      'end_timestamp' => $end_timestamp,
      'start_time_is_empty' => FALSE,
      'start_timestamp' => $start_timestamp,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['end_time_is_empty'] = DataDefinition::create('boolean')
      ->setLabel(t('End time is empty'));

    $properties['end_timestamp'] = DataDefinition::create('integer')
      ->setLabel(t('End timestamp'));

    $properties['start_time_is_empty'] = DataDefinition::create('boolean')
      ->setLabel(t('Start time is empty'))
      ->setRequired(TRUE);

    $properties['start_timestamp'] = DataDefinition::create('integer')
      ->setLabel(t('Start timestamp'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'start_timestamp' => [
          'size' => 'big',
          'type' => 'int',
          'unsigned' => FALSE,
        ],
        'start_time_is_empty' => [
          'size' => 'tiny',
          'type' => 'int',
        ],
        'end_timestamp' => [
          'size' => 'big',
          'type' => 'int',
          'unsigned' => FALSE,
        ],
        'end_time_is_empty' => [
          'size' => 'tiny',
          'type' => 'int',
        ],
      ],
      'indexes' => [
        'start_timestamp' => ['start_timestamp'],
        'end_timestamp' => ['end_timestamp'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['time_is_optional'] = [
      '#default_value' => $this->getSetting('time_is_optional'),
      '#title' => $this->t('Time is optional'),
      '#type' => 'checkbox',
    ];

    $element['end_is_optional'] = [
      '#default_value' => $this->getSetting('end_is_optional'),
      '#title' => $this->t('End is optional'),
      '#type' => 'checkbox',
    ];

    return $element;
  }

}
