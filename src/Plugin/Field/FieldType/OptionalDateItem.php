<?php

namespace Drupal\optional_date\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the optional_date field type.
 *
 * @FieldType(
 *   id = "optional_date",
 *   label = @Translation("Optional Date"),
 *   default_formatter = "optional_date",
 *   default_widget = "optional_date",
 * )
 */
class OptionalDateItem extends FieldItemBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'time_is_optional' => FALSE,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $timestamp = \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365);

    return [
      'time_is_empty' => FALSE,
      'timestamp' => $timestamp,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['time_is_empty'] = DataDefinition::create('boolean')
      ->setLabel(t('Time is empty'))
      ->setRequired(TRUE);

    $properties['timestamp'] = DataDefinition::create('integer')
      ->setLabel(t('Timestamp'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'timestamp' => [
          'size' => 'big',
          'type' => 'int',
          'unsigned' => FALSE,
        ],
        'time_is_empty' => [
          'size' => 'tiny',
          'type' => 'int',
        ],
      ],
      'indexes' => [
        'timestamp' => ['timestamp'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['time_is_optional'] = [
      '#default_value' => $this->getSetting('time_is_optional'),
      '#title' => $this->t('Time is optional'),
      '#type' => 'checkbox',
    ];

    return $element;
  }

}
