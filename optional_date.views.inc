<?php

/**
 * @file
 * Views hooks.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 */
function optional_date_field_views_data(FieldStorageConfigInterface $field_storage) {
  $data = [];
  $type = $field_storage->getType();

  if ($type == 'optional_date') {
    $data = optional_date_field_views_data_helper($field_storage, $data, 'timestamp');
  }
  elseif ($type == 'optional_date_range') {
    $data = optional_date_field_views_data_helper($field_storage, $data, 'end_timestamp');
    $data = optional_date_field_views_data_helper($field_storage, $data, 'start_timestamp');
  }

  return $data;
}

/**
 * Provides Views integration for any optional date-based fields.
 *
 * Overrides the default Views data for optional date-based fields, adding date
 * views plugins. Modules defining new optional date-based fields may use this
 * function to simplify Views integration.
 *
 * @param FieldStorageConfigInterface $field_storage
 *   The field storage config entity.
 * @param array $data
 *   Field view data or views_field_default_views_data($field_storage) if empty.
 * @param string $column_name
 *   The schema column name with the optional date value.
 *
 * @return array
 *   The array of field views data with the optional_date plugin.
 *
 * @see datetime_field_views_data()
 * @see datetime_range_field_views_data()
 */
function optional_date_field_views_data_helper(FieldStorageConfigInterface $field_storage, array $data, $column_name) {
  \Drupal::moduleHandler()->loadInclude('datetime', 'inc', 'datetime.views');

  $data = datetime_type_field_views_data_helper($field_storage, $data, $column_name);

  foreach ($data as $table_name => $table_data) {
    $data[$table_name][$field_storage->getName() . '_' . $column_name]['argument']['id'] = 'optional_date';
    $data[$table_name][$field_storage->getName() . '_' . $column_name]['filter']['id'] = 'optional_date';
    $data[$table_name][$field_storage->getName() . '_' . $column_name]['sort']['id'] = 'optional_date';
  }

  return $data;
}
